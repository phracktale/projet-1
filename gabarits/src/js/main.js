//=require ../../node_modules/jquery/dist/jquery.js
//=require ../../node_modules/slider-pro/dist/js/jquery.sliderPro.js
//=require ../../node_modules/jquery-modal/jquery.modal.js

$( document ).ready(function( $ ) {
		$(".link-menu-mobile").click(function() {
			event.preventDefault();
		  $(this).toggleClass("on");
		  $('.slider-pro').toggle();
		  	
		  if($(this).hasClass('on'))
		  {
		  	
		  	$('#mobile-search').addClass('active');
		  	$("#menu-principal").addClass('active');
		  }
		  else
		  {
		  	$('#mobile-search').removeClass('active');
		  	$("#menu-principal").removeClass('active');
		  }
		  
		  return false;
		});


		$( '.slider-pro' ).sliderPro({
			width: '100%',
			height: 300,
			arrows: true,
			buttons: false,
			autoplay: true,
    		waitForLayers: true,
    		autoScaleLayers: false
		});

		$( '#slider-rubrique' ).sliderPro({
			width: '100%',
			height: 200,
			arrows: false,
			buttons: false,
			autoplay: false,
    		waitForLayers: true,
    		autoScaleLayers: false,
    		touchSwipe:false
		});

		$(window).on("scroll", function() {
		    var fromTop = $("body").scrollTop();
		    var navpos = $('#navigation').position();
		    if(fromTop > navpos.top)
		    {
		    	$('#navigation').addClass("fixed");
		    	$('#navigation').removeClass("full-width");
		    	$('#header').removeClass("full-width");
		    	$('.logo-nav').show();
		    }
		    else
		    {
		    	$('#navigation').removeClass("fixed");
		    	$('#navigation').addClass("full-width");
		    	$('#header').addClass("full-width");
		    	$('.logo-nav').hide();
		    }
		    
		});

	});