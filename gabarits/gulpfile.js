// Requis
var gulp = require('gulp');

// Include plugins
var plugins = require('gulp-load-plugins')(); // tous les plugins de package.json

var extender = require('gulp-html-extend');
var critical = require('critical').stream;
var gulpsync = require('gulp-sync')(gulp);
var merge = require('merge2');

// Variables de chemins
var source = './src'; // dossier de travail
var modules = './node_modules'; // dossier de travail
var destination = './dist'; // dossier à livrer

// Tâche "build" = Build + minify
gulp.task('dev', gulpsync.sync(['html', 'css', 'scripts', 'img']));

// Tâche "prod" = toutes les tâches ensemble
gulp.task('prod', gulpsync.sync(['html', 'css-prod', 'scripts-prod', 'critical', 'imgmin']));
		


// Tâche par défaut
gulp.task('default', ['build']);


// Tâche "watch" = je surveille LESS et HTML
gulp.task('watch', function () {
  gulp.watch(source + '/css/*.scss', ['css-prod']);
  gulp.watch(source + '/*.html', ['html','css-prod']);
  gulp.watch(source + '/_includes/*.html', ['html', 'css-prod']);
});

// Tâche "html" = includes HTML
gulp.task('html', function() {
  return  gulp.src(source + '/*.html')
    // Generates HTML includes
    .pipe(extender({annotations: false,verbose: false})) // default options
    .pipe(gulp.dest(destination))
});


gulp.task('css-prod', function () {

  /* Stram principal avec tous les traitements sass, uncss */

  var styles =  gulp.src(source + '/css/styles.scss')
                .pipe(plugins.sass())
                .pipe(plugins.include())
                .pipe(plugins.csscomb())
                .pipe(plugins.cssbeautify({indent: '  '}))
                .pipe(plugins.autoprefixer())
                .pipe(plugins.uncss({
                  html: [
                    destination + '/*.html'
                  ],
                  ignore: [
                    '@font-face', 
                    ':hover', 
                    ':visited', 
                    ':focus',
                    '#navigation.fixed',
                    '.on'
                  ]
                }))
                .pipe(plugins.csso())
                .pipe(plugins.rename({suffix: '.min'}))
                .pipe(gulp.dest(destination + '/assets/css/'));
                
  /* streams secondaires qui sont déjà compilés et qu'il faut exclure du traitement uncss */
  
  var slider =  gulp.src(modules + '/slider-pro/dist/css/slider-pro.min.css');
  var modal =  gulp.src(modules + '/jquery-modal/jquery.modal.min.css');
  
  return merge(styles, slider, modal)
        .pipe(plugins.concat('styles.min.css'))
        .pipe(plugins.rename('styles.min.css'))
        .pipe(gulp.dest(destination + '/assets/css/'));

});


gulp.task('css', function () {
  return gulp.src(source + '/css/styles.scss')
    .pipe(plugins.sass())
    .pipe(plugins.include())
    .pipe(plugins.csscomb())
    .pipe(plugins.cssbeautify({indent: '  '}))
    .pipe(plugins.autoprefixer())
    .pipe(plugins.csso())
    .pipe(plugins.concat(modules + '/slider-pro/dist/css/slider-pro.min.css'))
    .pipe(plugins.concat(modules + '/jquery-modal/jquery.modal.min.css'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(destination + '/assets/css/'));
});

gulp.task('scripts-prod', function() {  
    return gulp.src(source + '/js/main.js')
      .pipe(plugins.include())
      .on('error', console.log)
      .pipe(plugins.uglify())
      .pipe(plugins.rename('main.min.js'))
      .pipe(gulp.dest(destination + '/assets/js/'));
});

gulp.task('scripts', function() {  
    return gulp.src(source + '/js/main.js')
      .pipe(plugins.include())
      .on('error', console.log)
      .pipe(plugins.rename('main.min.js'))
      .pipe(gulp.dest(destination + '/assets/js/'));
});


// Tâche "imgmin" = Images optimisées
gulp.task('imgmin', function () {
  return gulp.src(source + '/img/*.{gif,jpg,png,svg}')
    .pipe(plugins.imagemin({
        progressive: true,
        interlaced: true,
        svgoPlugins: [ {removeViewBox:false}, {removeUselessStrokeAndFill:false} ]
    }))
    .pipe(gulp.dest(destination + '/assets/img/'));
});

// Tâche "img" = Images optimisées
gulp.task('img', function () {
  return gulp.src(source + '/img/*.{gif,jpg,png,svg}')
    .pipe(gulp.dest(destination + '/assets/img/'));
});



// Tâche "critical" = critical inline CSS
gulp.task('critical', function() {
  return  gulp.src(destination + '/*.html')
    .pipe(critical({
      base: destination,
      inline: true,
      width: 320,
      height: 480,
      minify: true
    }))
    .pipe(gulp.dest(destination));
});