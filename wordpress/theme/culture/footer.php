		</main>
		<footer id="footer" role="contentinfo" class="pam full-width">
			<div class="wrapper">
				<div class="grid">
					<?php  dynamic_sidebar('widget-footer-right'); ?>
					<?php  dynamic_sidebar('widget-footer-center'); ?>
					<div>
						<h3>Suivez nous</h3>
						<?php 
							$options = get_option( 'identite_name' );
	   						$page_facebook = $options['page_facebook'];
	   						$page_twitter = $options['page_twitter'];
	   						$page_youtube = $options['page_youtube'];
	   					?>
		   					<a href="<?php echo $page_facebook; ?>" title="Page facebook de la ville" class="social"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-facebook.svg" alt="" width="64" height="64" /></a> <a href="<?php echo $page_twitter; ?>" title="Compte twitter de la ville" class="social"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-twitter.svg" alt="" width="64" height="64" /></a> 
		   					<a href="<?php echo $page_youtube; ?>" title="Page youTube de la ville" class="social"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-youtube.svg" alt="" width="64" height="64" /></a>
					</div>
					<?php  dynamic_sidebar('widget-footer-left'); ?>
					
					
					
				</div>
			</div>
		</footer>
		</div>
		<script async src="<?php echo get_template_directory_uri(); ?>/assets/js/main.min.js"></script>

	</body>
</html>