<?php
/*
Template Name: Sitemap
*/
?>

<?php get_header(); ?>

	<main role="main">
		<!-- SLIDER -->
			<?php get_template_part( 'blocks/page-slider', null ); ?>

		<!-- section -->
		<section class="layout">
			<div id="content">
			
				<header>
					<h2><span class="glyph glyph-points"></span><?php echo the_title(); ?></h2>
				</header>
				<article>
					<h2><?php _e('Pages', 'culture'); ?></h2>
					<ul><?php wp_list_pages("title_li=" ); ?></ul>
					

					<h2><?php _e('Flux RSS', 'culture'); ?></h2>
					<ul>
					<li><a title="Full content" href="feed:<?php bloginfo('rss2_url'); ?>"><?php _e('Flux RSS principal' , 'culture'); ?></a></li>
					
					</ul>
					<h2><?php _e('Catégories', 'culture'); ?></h2>
					<ul><?php wp_list_categories('sort_column=name&optioncount=1&hierarchical=0&feed=RSS'); ?></ul>
					<h2><?php _e('Actualités', 'culture'); ?></h2>
					<ul><?php $archive_query = new WP_Query('showposts=1000'); while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
					<li>
					<a href="<?php the_permalink() ?>" rel="bookmark" title="Lient permanent vers <?php the_title(); ?>"><?php the_title(); ?></a>
					(<?php comments_number('0', '1', '%'); ?>)
					</li>
					<?php endwhile; ?>
					</ul>
				</article>
			</div>

			<aside>
				<?php  dynamic_sidebar('widget-aside'); ?>
			</aside>

		</section>
		
		<!-- /section -->
	</main>

<?php get_footer(); ?>
