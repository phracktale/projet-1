<?php 
    $args = array(
        'post_type'   => 'caroussel',
        'meta_query'  => array(
        array(
                'value' => $post_id,
                'key' => 'rel_caroussel',
                'compare' => '='
            )
        )
    );

    $caroussel = new WP_Query( $args );
    
    if ( $caroussel->have_posts() ) {

?>
<div class="full-width">
    <h2 class="hide">Focus sur les activités culturelles</h2>
    <div id="slider-home" class="slider-pro">
        <div class="sp-slides">
        <?php  
                while ( $caroussel->have_posts() ) {
                    $caroussel->the_post(); 
        ?>

            <?php  $titre =  get_post_meta( get_the_ID(), 'titre_caroussel', TRUE ); ?>
            <?php  $texte = get_post_meta( get_the_ID(), 'texte_caroussel', TRUE ); ?>
            <?php  $lien =  get_post_meta( get_the_ID(), 'link_caroussel', TRUE ); ?>
            <!-- Slide <?php the_ID(); ?> -->
            <div class="sp-slide" id="post-<?php the_ID(); ?>">
                <img class="sp-image" src="<?php 
                    $image_id = get_post_thumbnail_id();
                    $image_url = wp_get_attachment_image_src($image_id,'full');
                    echo $image_url[0];
                    ?>"
                    alt="<?php the_title(); ?>" />
            
                    <?php if($lien != "") {  echo '<a href="' .  $lien . '">'; } ?>
                    <?php if($titre != "") { ?>
                    <div class="sp-layer sp-black sp-padding" data-horizontal="60" data-vertical="60" data-width="400" data-show-transition="left" data-show-delay="400">
                        <h3><?php  echo $titre; ?></h3>
                    </div>
                    <?php } ?>
                    <?php if($texte != "") { ?>
                    <div class="sp-layer sp-black sp-padding" data-horizontal="60" data-vertical="140" data-width="400" data-show-transition="left" data-show-delay="600">
                        <?php  echo $texte; ?>
                    </div>
                    <?php } ?>
                 <?php if($lien != "") {  echo '</a>'; } ?>
            </div>
<?php 
        } // endwhile; 
?>
        </div><!-- END Blog Roll --> 
    </div> <!-- END slider wrap -->
</div>
<?php 
    } //endif;
    wp_reset_query();
?>