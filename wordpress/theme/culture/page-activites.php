<?php
/**
 * Template Name: Activites
 */
?>
	
<?php $message = traitement_formulaire_inscription_evenement(); ?>

<?php get_header(); ?>

	<main role="main">
		<!-- SLIDER -->
			<?php  
				
				 while ( have_posts() ) : the_post();
					$post_id = get_the_ID();
					set_query_var( 'post_id', $post_id ); 
					get_template_part( 'blocks/page-slider', null ); 
				endwhile;
			?>

		<!-- section -->
		<section class="layout has-gutter">
			<div id="content">
				<header>
					<?php while ( have_posts() ) : the_post(); ?>
					<h2><span class="glyph glyph-points"></span><?php echo the_title(); ?></h2>
					<?php echo the_content(); ?>
					<?php endwhile; // end of the loop. ?>
				</header>
				
<?php 
			if($message != "")
			{
				echo $message;
			}
		  	
			$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			
			$current_year = isset($_GET['year']) ? $_GET['year'] : date('Y');
			$current_month = isset($_GET['month']) ? $_GET['month'] : date('m');

			$args = array(
				'post_type' 		=> 'activites',
				'meta_key'     	=> 'date_event',
		      	'meta_value'   	=> $current_year . '-' . $current_month .'-', 
		      	'meta_compare' 	=> 'RLIKE',
    			'posts_per_page' 	=> 5,
   				'orderby' 			=> 'meta_value_num',
    			'order' => 'DESC',
				'paged' => $paged
			);

			$wp_query = new WP_Query( $args); 
			
			if ($wp_query->have_posts()): 
?>
				<div class="body row-hover">
					<?php while ($wp_query->have_posts()) :  $wp_query->the_post(); ?>
							<?php get_template_part('loop-activites'); ?>
					<?php endwhile; // end of the loop. ?>

				<?php else: ?>
					<article>
						<header>
							<?php _e( 'Désolé, il n\'y a encore rien ici.', 'culture' ); ?>
						</header>
						<?php echo the_content(); ?>
					</article>
				<?php endif; ?>
				<!-- pagination -->
				<div class="pagination">
					<?php html5wp_pagination(); ?>
				</div>
				<!-- /pagination -->
								
					</div>
					
				</section>
				<!-- /section -->
			</main>
		<?php
		 wp_reset_query();
		get_footer(); 
		?>