<?php get_header(); ?>

	<main role="main">
	

		<!-- section -->
	<section class="layout has-gutter">
			<div id="content">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<header>
			<h2><span class="glyph glyph-points"></span><?php echo the_title(); ?></h2>
			<hr class="separateur" />
			<span class="author"><?php _e( 'publié par', 'culture' ); ?> le Plessis-Belleville</span> - 
			<span class="date"><?php the_time('j F Y'); ?></span>	
				
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
				<div class="logo"><?php the_post_thumbnail(array(300,300)); // Fullsize image for the single post ?></div>
			<?php endif; ?>
			<!-- /post thumbnail -->
			<!-- /post details -->

			<?php the_content(); // Dynamic Content ?>

			
			

			
			<hr class="separateur" />
			<?php comments_template(); ?>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'naturespace' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>
		</div>

		<aside>
			<?php  dynamic_sidebar('widget-aside'); ?>
		</aside>
	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
