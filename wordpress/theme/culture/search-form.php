<form method="get" action="<?php echo home_url(); ?>" role="search">
	<a name="champRecherche" class="hide"><h2>Recherche</h2></a>
	<hr class="hide">
	<div class="input-group  txtright pam">
      <label for="s" class="hide">Votre recherche</label><input type="text" name="s" class="form-control" placeholder="Votre recherche...">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button">Go!</button>
      </span>
    </div>
</form>