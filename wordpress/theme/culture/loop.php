<?php while ( have_posts() ) : the_post(); ?>
					
						<!-- article -->
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							
							<!-- post thumbnail -->
							<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<div class="logo">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_post_thumbnail(array(230,230) ); // Declare pixel size you need inside the array ?>
									</a>
								</div>
							<?php endif; ?>
							<!-- /post thumbnail -->
								<div class="presentation">
									<a href="<?php the_permalink(); ?>" >
									<h3><?php the_title(); ?></h3>
									
									<span class="author"><?php _e( 'publié par', 'culture' ); ?> le Plessis-Belleville</span>
									<span class="date"><?php _e( 'le', 'culture' ); ?>  <?php the_time('j F Y'); ?> </span>
		
									<?php the_excerpt(); ?>
									</a>
									<a href="<?php the_permalink(); ?>" class="btn btn-primary fr mtl	"><?php _e( 'En savoir plus', 'culture' ); ?></a>
								</div>
							
						</article>
						
								
					<?php endwhile; // end of the loop. ?>