<?php
/**
 * Template Name: Accueil
 */
?>
<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
			<!-- SLIDER -->
			<?php  
				
				 while ( have_posts() ) : the_post();
					$post_id = get_the_ID();
					set_query_var( 'post_id', $post_id ); 
					get_template_part( 'blocks/home-slider', null ); 
				endwhile;
			?>

			<?php while ( have_posts() ) : the_post(); ?>
				
				<?php the_content(); ?>
						
			<?php endwhile; // end of the loop. ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>