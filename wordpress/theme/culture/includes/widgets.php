<?php

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
	// Define Sidebar Widget Area 1
     register_sidebar(array(
        'name' => __('Colonne droite', 'culture'),
        'description' => __('', 'culture'),
        'id' => 'widget-aside',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Pied de page droit', 'culture'),
        'description' => __('', 'culture'),
        'id' => 'widget-footer-right',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => __('Pied de page centre', 'culture'),
        'description' => __('', 'culture'),
        'id' => 'widget-footer-center',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => __('Pied de page gauche', 'culture'),
        'description' => __('', 'culture'),
        'id' => 'widget-footer-left',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}