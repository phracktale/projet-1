<?php

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcodes
add_shortcode('bloc', 'shortcode_bloc'); 
add_shortcode('separateur', 'shortcode_separateur');
add_shortcode('row', 'shortcode_row'); 
add_shortcode('btn', 'shortcode_btn');

 


// Shortcodes above would be nested like this -
// [row] [vignette] Here's the page title! [/vignette] [/row]

// Shortcode Demo with Nested Capability
function shortcode_row($atts, $content = null)
{
    return '<div class="row">' . do_shortcode($content) . '</row>'; // do_shortcode allows for nested Shortcodes
}
function shortcode_bloc($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    $att = shortcode_atts( array(
        'fonction' => 'article',
        'display' => 'grid',
        'col' => 0,
        'post_type' => 'post',
        'titre' => '',
        'lien' => '',
        'texte_lien' => '',
        'animation' => 'zoom',
        'affiche_description' => 'oui',
        'lien_vignette' => 'oui'
    ), $atts );


    switch($att['fonction'])
    {
        case "article":
            $args['post_type'] = $att['post_type'];
            if($att['col'] > 0)
            {
                $args['posts_per_page'] = $att['col'];
            }

            $query = new WP_Query( $args );

            if ( $query->have_posts() ) {
               $bloc = '';
                if($att['titre'] != "")
                {
                   $bloc .= ' <h2 class="txtcenter"><span class="glyph glyph-points"></span>' . $att['titre'] . '</h2>';
                }
                    
                $bloc .= do_shortcode($content) ;

               if($att['display'] == "grid")
               {
                    $class_display = 'grid-' . $att['col'] . '-small-2-tiny-1 has-gutter flex-centered';
               }
               elseif($att['display'] == "list")
               {
                    $class_display = 'row';
               }

               $bloc .= '<div class="' . $class_display . '">';
               $posts = $query->posts;

               foreach($posts as $post) 
               {
                    if (has_post_thumbnail( $post->ID) ) {
                        $vignette_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'grid3-thumbnail' );
                        if ( !empty( $vignette_url[0] ) ) {
                           $image_url = $vignette_url[0];
                        }
                        else
                        {
                            $image_url = '//dummyimage.com/400x400/4ca244/fff.png';
                        }
                    }
                    else
                    {
                        $image_url = '//dummyimage.com/400x400/4ca244/fff.png';
                    }

                    $tax = get_object_taxonomies( $att['post_type'], 'objects' );

                    $terms = get_the_terms( $post->ID, $tax['category']->name);
                    $term = array_pop($terms);

                    $date_event = get_post_meta($post->ID, 'date_event', true);

                   
                    $content = $post->post_excerpt != "" ? $post->post_excerpt : wp_trim_all_excerpt($post->post_content);
                    if($att['affiche_description'] == "non")
                    {
                        $content = "";
                    }
                    $bloc .= bloc_template($post->slug, $att['display'], $image_url, $post->post_title, $content, $term->name, $date_event, $att['animation'], $att['lien_vignette'], get_permalink($post));
                
                }
                $bloc .= '</div>';
                if($att['lien'] != "")
                {
                  $bloc .= '<p class="txtcenter"><a href="' . $att['lien'] . '" class="btn btn-large btn-primary">' . $att['texte_lien'] . '</a></p>';
                }
                wp_reset_postdata();
            }
            break;

        case "categorie":
            $animation = 'categorie';

            $taxonomies = get_object_taxonomies($att['post_type'], 'objects');

            $taxonomy = $taxonomies['categorie_activites']->name;
            $terms = get_terms([
                'taxonomy' => $taxonomy,
                'hide_empty' => false,
            ]);

            $count=0;

            $bloc = '';
            if($att['titre'] != "")
            {
               $bloc .= ' <h2 class="txtcenter"><span class="glyph glyph-points"></span>' . $att['titre'] . '</h2>';
            }
                
            $bloc .= do_shortcode($content) ;

           if($att['display'] == "grid")
           {
                $class_display = 'grid-' . $att['col'] . '-small-2-tiny-1 has-gutter';
           }
           elseif($att['display'] == "list")
           {
                $class_display = 'row';
           }

           $bloc .= '<div class="' . $class_display . '">';

           
            foreach($terms as $term)
            {
                if (function_exists('z_taxonomy_image_url'))
                {
                    $image_url =  z_taxonomy_image_url($term->term_id, 'grid6-thumbnail');
                    if($image_url == "")
                    {
                        $image_url = '//dummyimage.com/200x200/4ca244/fff.png';
                    }
                } 
                else
                {
                    $image_url = '//dummyimage.com/200x200/4ca244/fff.png';
                }

                $content = $term->description;

                if($att['affiche_description'] == "non")
                {
                    $content = "";
                }
                $bloc .= bloc_template($term->slug, $att['display'], $image_url, $term->name, $content, null, null, $att['animation'], $att['lien_vignette'], $att['lien'] . "#" . $term->slug);

                if($att['col'] > 0)
                {
                    $count++;
                    if($count >= $att['col'])
                    {
                        break;
                    }
                }
            } 
            $bloc .= '</div>';
           
            if($att['lien'] != "")
            {
              $bloc .= '<p class="txtcenter"><a href="' . $att['lien'] . '" class="btn btn-large btn-primary">' . $att['texte_lien'] . '</a></p>';
            }
            
            break;
    }
    return $bloc;
}

function bloc_template($url,$display, $image_url, $titre, $content, $term_name, $date_event, $animation, $lien_vignette, $link)
{
    $bloc .= '<div id="' . $url . '" class="item item-' . $animation . ' mbl">';
    if($lien_vignette == 'oui') {
        $bloc .= '<a href="' . $link . '">';
    }

    $bloc .= '<div class="wrapper-img">';
    $bloc .= '<div><img src="' . $image_url . '" alt="' . $titre . '" /></div>';
    if($term_name!= "")
    {
        $bloc .= '<div class="badge">' . $term_name . '</div>';
    }

    $bloc .= '<div class="caption">';

    if($date_event != "")
    {
        $bloc .= '<div class="date">' .$date_event . '</div>';
    }
                                    
    $bloc .= '<h3>' . $titre . '</h3>';
    $bloc .= $content;
                     
                   
    $bloc .= '</div></div>';
    if($lien_vignette == 'oui') {
        $bloc .= '</a>';
    }
    $bloc .= '<hr class="hide"></div>';
    return $bloc;
}


function shortcode_btn($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    $att = shortcode_atts( array(
        'class' => '',
        'href' => '',
        'align' => ''

    ), $atts );

    if($att['align'] == "center"){$align = "text-center";}
    if($att['align'] == "right"){$align = "pull-right";}
    return '<div class="wrapper ' . $align . '"><a href="' . $att['href'] .'" class="btn btn-large btn-' . $att['class'] .'">' . do_shortcode($content) . '<span class="icon_dot"></span></a></div>';
}

function shortcode_separateur($atts, $content = null)
{
    return '<hr class="separ" />';
}



remove_shortcode('gallery');

add_shortcode('gallery', 'responsiveSlides_gallery');

/**
 * Builds the Gallery shortcode output.
 *
 * This implements the functionality of the Gallery Shortcode for displaying
 * WordPress images on a post.
 *
 * @since 2.5.0
 *
 * @staticvar int $instance
 *
 * @param array $attr {
 *     Attributes of the gallery shortcode.
 *
 *     @type string       $order      Order of the images in the gallery. Default 'ASC'. Accepts 'ASC', 'DESC'.
 *     @type string       $orderby    The field to use when ordering the images. Default 'menu_order ID'.
 *                                    Accepts any valid SQL ORDERBY statement.
 *     @type int          $id         Post ID.
 *     @type string       $itemtag    HTML tag to use for each image in the gallery.
 *                                    Default 'dl', or 'figure' when the theme registers HTML5 gallery support.
 *     @type string       $icontag    HTML tag to use for each image's icon.
 *                                    Default 'dt', or 'div' when the theme registers HTML5 gallery support.
 *     @type string       $captiontag HTML tag to use for each image's caption.
 *                                    Default 'dd', or 'figcaption' when the theme registers HTML5 gallery support.
 *     @type int          $columns    Number of columns of images to display. Default 3.
 *     @type string|array $size       Size of the images to display. Accepts any valid image size, or an array of width
 *                                    and height values in pixels (in that order). Default 'thumbnail'.
 *     @type string       $ids        A comma-separated list of IDs of attachments to display. Default empty.
 *     @type string       $include    A comma-separated list of IDs of attachments to include. Default empty.
 *     @type string       $exclude    A comma-separated list of IDs of attachments to exclude. Default empty.
 *     @type string       $link       What to link each image to. Default empty (links to the attachment page).
 *                                    Accepts 'file', 'none'.
 * }
 * @return string HTML content to display gallery.
 */
function responsiveSlides_gallery( $attr ) {
    $post = get_post();

    static $instance = 0;
    $instance++;

    if ( ! empty( $attr['ids'] ) ) {
        // 'ids' is explicitly ordered, unless you specify otherwise.
        if ( empty( $attr['orderby'] ) ) {
            $attr['orderby'] = 'post__in';
        }
        $attr['include'] = $attr['ids'];
    }

    /**
     * Filters the default gallery shortcode output.
     *
     * If the filtered output isn't empty, it will be used instead of generating
     * the default gallery template.
     *
     * @since 2.5.0
     * @since 4.2.0 The `$instance` parameter was added.
     *
     * @see gallery_shortcode()
     *
     * @param string $output   The gallery output. Default empty.
     * @param array  $attr     Attributes of the gallery shortcode.
     * @param int    $instance Unique numeric ID of this gallery shortcode instance.
     */
    $output = apply_filters( 'post_gallery', '', $attr, $instance );
    if ( $output != '' ) {
        return $output;
    }

    $html5 = current_theme_supports( 'html5', 'gallery' );
    $atts = shortcode_atts( array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post ? $post->ID : 0,
        'itemtag'    => $html5 ? 'li'     : 'li',
        'icontag'    => $html5 ? 'div'        : 'div',
        'captiontag' => $html5 ? 'div' : 'div',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => '',
        'link'       => ''
    ), $attr, 'gallery' );

    $id = intval( $atts['id'] );

    if ( ! empty( $atts['include'] ) ) {
        $_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( ! empty( $atts['exclude'] ) ) {
        $attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
    } else {
        $attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
    }

    if ( empty( $attachments ) ) {
        return '';
    }

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment ) {
            $output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
        }
        return $output;
    }

    $itemtag = tag_escape( $atts['itemtag'] );
    $captiontag = tag_escape( $atts['captiontag'] );
    $icontag = tag_escape( $atts['icontag'] );
    $valid_tags = wp_kses_allowed_html( 'post' );
    if ( ! isset( $valid_tags[ $itemtag ] ) ) {
        $itemtag = 'li';
    }
    if ( ! isset( $valid_tags[ $captiontag ] ) ) {
        $captiontag = 'div';
    }
    if ( ! isset( $valid_tags[ $icontag ] ) ) {
        $icontag = 'div';
    }

    $columns = intval( $atts['columns'] );
    $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";

    $gallery_style = '';

    /**
     * Filters whether to print default gallery styles.
     *
     * @since 3.1.0
     *
     * @param bool $print Whether to print default gallery styles.
     *                    Defaults to false if the theme supports HTML5 galleries.
     *                    Otherwise, defaults to true.
     */
    if ( apply_filters( 'use_default_gallery_style', ! $html5 ) ) {
        $gallery_style = "
        <style type='text/css'>
            #{$selector} {
                margin: auto;
            }
            #{$selector} .gallery-item {
                float: {$float};
                margin-top: 10px;
                text-align: center;
                width: {$itemwidth}%;
            }
            #{$selector} img {
                //border: 2px solid #cfcfcf;
            }
            #{$selector} .gallery-caption {
                margin-left: 0;
            }
            /* see gallery_shortcode() in wp-includes/media.php */
        </style>\n\t\t";
    }

    $size_class = sanitize_html_class( $atts['size'] );
    $gallery_div = "<div class='rslides_container'><ul id='$selector' class='rslides gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

    /**
     * Filters the default gallery shortcode CSS styles.
     *
     * @since 2.5.0
     *
     * @param string $gallery_style Default CSS styles and opening HTML div container
     *                              for the gallery shortcode output.
     */
    $output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );

    $i = 0;
    foreach ( $attachments as $id => $attachment ) {

        $attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
        if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
            $image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
        } elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
            $image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
        } else {
            $image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
        }
        $image_meta  = wp_get_attachment_metadata( $id );

        $orientation = '';
        if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
            $orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
        }
        $output .= "<{$itemtag} class='gallery-item'>";
        $output .= "
            <{$icontag} class='gallery-icon {$orientation}'>
                $image_output
            </{$icontag}>";
        if ( $captiontag && trim($attachment->post_excerpt) ) {
            $output .= "
                <{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
                " . wptexturize($attachment->post_excerpt) . "
                </{$captiontag}>";
        }
        $output .= "</{$itemtag}>";
        if ( ! $html5 && $columns > 0 && ++$i % $columns == 0 ) {
            //$output .= '<br style="clear: both" />';
        }
    }

    if ( ! $html5 && $columns > 0 && $i % $columns !== 0 ) {
        //$output .= "<br style='clear: both' />";
    }

    $output .= "</ul><br style='clear: both' /></div>\n";
    return $output;
}



/*
    Shortcode UI
*/


add_action('init', 'add_button');
function add_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_plugin');
     add_filter('mce_buttons', 'register_button');
   }
}

function register_button($buttons) {
   array_push($buttons, "bloc", "separateur");
   return $buttons;
}

function add_plugin($plugin_array) {
   $plugin_array['culture'] = get_bloginfo('template_url').'/js/shortcodeUI.js';

   return $plugin_array;
}