<?php
class Linker {

    protected $_already_saved = false;  # Used to avoid saving twice

    public function __construct() {
        $this->do_initialize();
    }

	protected function do_initialize() {

        add_action(
            'save_post',
            array( $this, 'save_meta_box_data' ),
            10,
            2
        );

        add_action(
            "add_meta_boxes_for_development_area",
            array( $this, 'setup_development_area_boxes' )
        );

        add_action(
            "add_meta_boxes_for_property",
            array( $this, 'setup_property_boxes' )
        );

    }

    # Development area post type
    # - this post type can have multiple related properties posts
    public function setup_development_area_boxes( \WP_Post $post ) {
        add_meta_box(
            'area_related_properties_box',
            __('Related Properties', 'language'),
            array( $this, 'draw_area_related_properties_box' ),
            $post->post_type,
            'advanced',
            'default'
        );
    }

    public function draw_area_related_properties_box( \WP_Post $post ) {

        $all_properties = $this->get_all_of_post_type( 'property' );

        $linked_property_ids = $this->get_linked_property_ids( $post->ID );

        if ( 0 == count($all_properties) ) {
            $choice_block = '<p>No properties currently in system.</p>';
        } else {
            $choices = array();
            foreach ( $all_properties as $property ) {
                $checked = ( in_array( $property->ID, $linked_property_ids ) ) ? ' checked="checked"' : '';

                $display_name = esc_attr( $property->post_title );
                $choices[] = <<<HTML
<label><input type="checkbox" name="property_ids[]" value="{$property->ID}" {$checked}/> {$display_name}</label>
HTML;

            }
            $choice_block = implode("\r\n", $choices);
        }

        # Make sure the user intended to do this.
        wp_nonce_field(
            "updating_{$post->post_type}_meta_fields",
            $post->post_type . '_meta_nonce'
        );

        echo $choice_block;
    }

    # Grab all posts of the specified type
    # Returns an array of post objects
    protected function get_all_of_post_type( $type_name = '') {
        $items = array();
        if ( !empty( $type_name ) ) {
            $args = array(
                'post_type' => "{$type_name}",
                'posts_per_page' => -1,
                'order' => 'ASC',
                'orderby' => 'title'
            );
            $results = new \WP_Query( $args );
            if ( $results->have_posts() ) {
                while ( $results->have_posts() ) {
                    $items[] = $results->next_post();
                }
            }
        }
        return $items;
    }

    protected function get_linked_property_ids( $area_id = 0 ) {
        $ids = array();
        if ( 0 < $area_id ) {
            $args = array(
                'post_type' => 'property',
                'posts_per_page' => -1,
                'order' => 'ASC',
                'orderby' => 'title',
                'meta_query' => array(
                    array(
                        'key' => '_development_area_id',
                        'value' => (int)$area_id,
                        'type' => 'NUMERIC',
                        'compare' => '='
                    )
                )
            );
            $results = new \WP_Query( $args );
            if ( $results->have_posts() ) {
                while ( $results->have_posts() ) {
                    $item = $results->next_post();
                    $ids[] = $item->ID;
                }
            }
        }
        return $ids;
    }

    # Post type metabox setup
    public function setup_property_boxes( \WP_Post $post ) {
        add_meta_box(
            'property_linked_area_box',
            __('Related Development Area', 'language'),
            array( $this, 'draw_property_linked_area_box' ),
            $post->post_type,
            'advanced',
            'default'
        );
    }

     public function draw_property_linked_area_box( \WP_Post $post ) {

        $all_areas = $this->get_all_of_post_type('development_area');

        $related_area_id = $this->get_property_linked_area_id( $post->ID );

        if ( 0 == $all_areas ) {
            $choice_block = '<p>No development areas to choose from yet.</p>';
        } else {
            $choices = array();
            $selected = ( 0 == $related_area_id )? ' selected="selected"':'';
            $choices[] = '<option value=""' . $selected . '> -- None -- </option>';
            foreach ( $all_areas as $area ) {
                $selected = ( $area->ID == (int)$related_area_id ) ? ' selected="selected"' : '';

                $display_name = esc_attr( $area->post_title );
                $choices[] = <<<HTML
<option value="{$area->ID}" {$selected}>{$display_name}</option>
HTML;

            }
            $choice_list = implode("\r\n" . $choices);
            $choice_block = <<<HTML
<select name="development_area_id">
{$choice_list}
</select>
HTML;

        }

        wp_nonce_field(
            "updating_{$post->post_type}_meta_fields",
            $post->post_type . '_meta_nonce'
        );

        echo $choice_block;
    }

    protected function get_property_linked_area_id( $property_id = 0 ) {
	    $area_id = 0;
	    if ( 0 < $property_id ) {
	        $area_id = (int) get_post_meta( $property_id, '_development_area_id', true );
	    }
	    return $area_id;
	}

	public function save_meta_box_data( $post_id = 0, \WP_Post $post = null ) {

        $do_save = true;

        $allowed_post_types = array(
            'development_area',
            'property'
        );

        # Do not save if we have already saved our updates
        if ( $this->_already_saved ) {
            $do_save = false;
        }

        # Do not save if there is no post id or post
        if ( empty($post_id) || empty( $post ) ) {
            $do_save = false;
        } else if ( ! in_array( $post->post_type, $allowed_post_types ) ) {
            $do_save = false;
        }

        # Do not save for revisions or autosaves
        if (
            defined('DOING_AUTOSAVE')
            && (
                is_int( wp_is_post_revision( $post ) )
                || is_int( wp_is_post_autosave( $post ) )
            )
        ) {
            $do_save = false;
        }

        # Make sure proper post is being worked on
        if ( !array_key_exists('post_ID', $_POST) || $post_id != $_POST['post_ID'] ) {
            $do_save = false;
        }

        # Make sure we have the needed permissions to save [ assumes both types use edit_post ]
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            $do_save = false;
        }

        # Make sure the nonce and referrer check out.
        $nonce_field_name = $post->post_type . '_meta_nonce';
        if ( ! array_key_exists( $nonce_field_name, $_POST) ) {
            $do_save = false;
        } else if ( ! wp_verify_nonce( $_POST["{$nonce_field_name}"], "updating_{$post->post_type}_meta_fields" ) ) {
            $do_save = false;
        } else if ( ! check_admin_referer( "updating_{$post->post_type}_meta_fields", $nonce_field_name ) ) {
            $do_save = false;
        }

        if ( $do_save ) {
            switch ( $post->post_type ) {
                case "development_area":
                    $this->handle_development_area_meta_changes( $post_id, $_POST );
                    break;
                case "property":
                    $this->handle_property_meta_changes( $post_id, $_POST );
                    break;
                default:
                    # We do nothing about other post types
                    break;
            }

            # Note that we saved our data
            $this->_already_saved = true;
        }
        return;
    }

    # Development areas can link to multiple properties
    # but each property can only link to a single development area
    protected function handle_development_area_meta_changes( $post_id = 0, $data = array() ) {

        # Get the currently linked property ids for this development area
        $linked_property_ids = $this->get_linked_property_ids( $post_id );

        # Get the list of property ids checked when the user saved changes
        if ( array_key_exists('property_ids', $data) && is_array( $data['property_ids'] ) ) {
            $chosen_property_ids = $data['property_ids'];
        } else {
            $chosen_property_ids = array();
        }

        # Build a list of properties to be linked or unlinked from this area
        $to_remove = array();
        $to_add = array();

        if ( 0 < count( $chosen_property_ids ) ) {
            # The user chose at least one property to link to
            if ( 0 < count( $linked_property_ids ) ) {
                # We already had at least one property linked

                # Cycle through existing and note any that the user did not have checked
                foreach ( $linked_property_ids as $property_id ) {
                    if ( ! in_array( $property_id, $chosen_property_ids ) ) {
                        # Currently linked, but not chosen. Remove it.
                        $to_remove[] = $property_id;
                    }
                }

                # Cycle through checked and note any that are not currently linked
                foreach ( $chosen_property_ids as $property_id ) {
                    if ( ! in_array( $property_id, $linked_property_ids ) ) {
                        # Chosen but not in currently linked. Add it.
                        $to_add[] = $property_id;
                    }
                }

            } else {
                # No previously chosen ids, simply add them all
                $to_add = $chosen_property_ids;
            }

        } else if ( 0 < count( $linked_property_ids ) ) {
            # No properties chosen to be linked. Remove all currently linked.
            $to_remove = $linked_property_ids;
        }

        if ( 0 < count($to_add) ) {
            foreach ( $to_add as $property_id ) {
                # This will overwrite any existing value for the linking key
                # to ensure we maintain only one dev area linked by each property.
                update_post_meta( $property_id, '_development_area_id', $post_id );
            }
        }

        if ( 0 < count( $to_remove ) ) {
            foreach ( $to_remove as $property_id ) {
                # This will delete all existing linked areas for the property
                # to ensure we only ever have one linked area per property
                delete_post_meta( $property_id, '_development_area_id' );
            }
        }
    }

    # Properties only relate to a single development area
    protected function handle_property_meta_changes( $post_id = 0, $data = array() ) {

        # Get any currently linked development area
        $linked_area_id = $this->get_property_linked_area_id( $post_id );
        if ( empty($linked_area_id) ) {
            $linked_area_id = 0;
        }

        if ( array_key_exists( 'development_area_id', $data ) && !empty($data['development_area_id'] ) ) {
            $received_area_id = (int)$data['development_area_id'];
        } else {
            $received_area_id = 0;
        }

        if ( $received_area_id != $linked_area_id ) {
            # This will overwrite any and all existing copies of our meta key
            # so we can ensure we only have one linked area per property
            update_post_meta( $post_id, '_development_area_id', $received_area_id );
        }
    }
}