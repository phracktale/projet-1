<?php

/* 
	METABOX
*/


add_action( 'admin_init', 'add_meta_boxes' );
add_action( 'save_post', 'save_date_event_field' );
add_action( 'save_post', 'save_titre_caroussel_field' );
add_action( 'save_post', 'save_texte_caroussel_field' );
add_action( 'save_post', 'save_link_caroussel_field' );
add_action( 'save_post', 'save_rel_caroussel_field' );


function add_meta_boxes() {
    add_meta_box( 'date_event', 'Date de l\'évènement', 'date_event_field', 'activites' );
    add_meta_box( 'date_event', 'Date de l\'activité', 'date_event_field', 'post' );
    add_meta_box( 'titre_caroussel', 'Titre de la vignette', 'titre_caroussel_field', 'caroussel' );
    add_meta_box( 'texte_caroussel', 'Texte de la vignette', 'texte_caroussel_field', 'caroussel' );
    add_meta_box( 'link_caroussel', 'Lien de la diapo:', 'link_caroussel_field', 'caroussel');
    add_meta_box( 'rel_caroussel', 'Caroussel visible sur la page:', 'rel_caroussel_field', 'caroussel');
}   

function date_event_field() {
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'var_nonce');
    $val = get_post_meta($post->ID, 'date_event', true);
    echo '<input type="date" name="date_event" value="' . $val . '" />';
}

function save_date_event_field( $post_id ) {
    // verify nonce
    if ( empty( $_POST['var_nonce'] )  || !wp_verify_nonce( $_POST['var_nonce'], basename( __FILE__ ) ) ){return $post_id;}
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){return $post_id; }
    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ){return $post_id; }

    update_post_meta( $post_id, 'date_event', $_POST['date_event']) ;
}


function titre_caroussel_field() {
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'var_nonce');
    $val = get_post_meta($post->ID, 'titre_caroussel', true);
    echo '<input type="text" name="titre_caroussel" style="width:100%" value="' . $val . '" />';
}

function save_titre_caroussel_field( $post_id ) {
    // verify nonce
    if ( empty( $_POST['var_nonce'] )  || !wp_verify_nonce( $_POST['var_nonce'], basename( __FILE__ ) ) ){return $post_id;}
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){return $post_id; }
    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ){return $post_id; }

    update_post_meta( $post_id, 'titre_caroussel', $_POST['titre_caroussel']) ;
}

function texte_caroussel_field() {
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'var_nonce');
    $val = get_post_meta($post->ID, 'texte_caroussel', true);
    echo '<textarea name="texte_caroussel" col="50" row="4" style="width:100%" >' . $val . '</textarea>';
}

function save_texte_caroussel_field( $post_id ) {
    // verify nonce
    if ( empty( $_POST['var_nonce'] )  || !wp_verify_nonce( $_POST['var_nonce'], basename( __FILE__ ) ) ){return $post_id;}
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){return $post_id; }
    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ){return $post_id; }

    update_post_meta( $post_id, 'texte_caroussel', $_POST['texte_caroussel']) ;
}

function link_caroussel_field() {
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'var_nonce');
    $val = get_post_meta($post->ID, 'link_caroussel', true);
    echo '<input type="text" name="link_caroussel" style="width:100%" value="' . $val . '" placeholder="http://www.site.fr" />';
}

function save_link_caroussel_field( $post_id ) {
    // verify nonce
    if ( empty( $_POST['var_nonce'] )  || !wp_verify_nonce( $_POST['var_nonce'], basename( __FILE__ ) ) ){return $post_id;}
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){return $post_id; }
    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ){return $post_id; }

    update_post_meta( $post_id, 'link_caroussel', $_POST['link_caroussel']) ;
}

function rel_caroussel_field() {
    global $post;
    wp_nonce_field( basename( __FILE__ ), 'var_nonce');
    $val = get_post_meta($post->ID, 'rel_caroussel', true);


    $args = array(
        'depth'                 => 0,
        'child_of'              => 0,
        'selected'              => $val,
        'echo'                  => 1,
        'name'                  => 'rel_caroussel',
        'id'                    => null, // string
        'class'                 => null, // string
        'show_option_none'      => "Pas de lien", // string
        'show_option_no_change' => null, // string
        'option_none_value'     => null, // string
    );

    wp_dropdown_pages( $args );

    //echo '<input type="text" id="rel_caroussel" name="rel_caroussel" style="width:100%" value="'.$val.'" />';
}


function save_rel_caroussel_field( $post_id ) {
   
   if ( 'caroussel' != get_post_type( $post_id ) ) {return $post_id; }

    // verify nonce
    if ( empty( $_POST['var_nonce'] )  || !wp_verify_nonce( $_POST['var_nonce'], basename( __FILE__ ) ) ){return $post_id;}
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){return $post_id; }
    // check permissions
    if ( !current_user_can( 'edit_post', $post_id ) ){return $post_id; }
   
    update_post_meta( $post_id, 'rel_caroussel', $_POST['rel_caroussel'] );
}
