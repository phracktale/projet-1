<?php

add_action( 'init', 'my_excerpt' );
function my_excerpt() {
     add_post_type_support( 'page', 'excerpt' );
}


/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

add_action('init', 'create_post_type');

function create_post_type()
{
    register_taxonomy( 'categorie_activites', 'activites', array( 'hierarchical' => true, 'label' => 'Catégorie', 'query_var' => true, 'rewrite' => true ) );
    
    register_post_type('activites', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => 'Activités', // Rename these to suit
            'singular_name' => 'Activité',
            'add_new' => 'Ajouter nouveau',
            'add_new_item' => 'Ajouter nouvelle activité',
            'edit' => 'Editer',
            'edit_item' => 'Editer',
            'new_item' => 'Nouvelle activité',
            'view' => 'Voir',
            'view_item' => 'Voir activité',
            'search_items' => 'Recherche',
            'not_found' => 'Non trouvé',
            'not_found_in_trash' => 'Non trouvé dans la poubelle'
        ),
        'public' => true,
        'publicly_queryable' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array('title','editor','excerpt','thumbnail'), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true
    ));


     register_post_type( 'caroussel',
        array(
            'labels' => array(
                'name' => __( 'Caroussels' ),
                'singular_name' => __( 'Caroussel' )
            ),
            'supports' => array( 'title', 'thumbnail' ),
            'has_archive' => true,
            'query_var' => true,
            'can_export' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'public' => true
        )
      );


}