<?php
add_action('init', 'culture_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'culture_conditional_scripts'); // Add Conditional Page Scripts
add_action('wp_enqueue_scripts', 'culture_styles'); // Add Theme Stylesheet


function culture_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

    	/*
        wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0');
        wp_enqueue_script('conditionizr');
        */

    }
}

function culture_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        /*
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname');
        */
    }
}

function culture_styles()
{
   

     /*
     wp_register_style('responsive', get_template_directory_uri() . '/assets/css/responsive.css', array(), '1.0', 'all');
     wp_enqueue_style('responsive');
     */
}