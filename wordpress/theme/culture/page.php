<?php get_header(); ?>

	<main role="main">
		<!-- SLIDER -->
			<?php  
				
				 while ( have_posts() ) : the_post();
					$post_id = get_the_ID();
					set_query_var( 'post_id', $post_id ); 
					get_template_part( 'blocks/page-slider', null ); 
				endwhile;
			?>

		<!-- section -->
		<section class="layout has-gutter">
			<div id="content">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<header>
					<h2><span class="glyph glyph-points"></span><?php echo the_title(); ?></h2>
				</header>
				<article>
					<?php echo the_content(); ?>
				</article>
				<?php endwhile; // end of the loop. ?>
				<?php else: ?>
					<header>
						<?php _e( 'Désolé, il n\'y a encore rien ici.', 'culture' ); ?>
					</header>
					<article>
						<?php echo the_content(); ?>
					</article>

				<?php endif; ?>
			</div>

			<aside>
				<?php  dynamic_sidebar('widget-aside'); ?>
			</aside>

		</section>
		
		<!-- /section -->
	</main>

<?php get_footer(); ?>
