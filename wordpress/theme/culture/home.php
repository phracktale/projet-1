<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section>
			<!-- SLIDER -->
			<?php get_template_part( 'blocks/home-slider', null ); ?>

			<?php while ( have_posts() ) : the_post(); ?>
				
				<?php the_content(); ?>
						
			<?php endwhile; // end of the loop. ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>