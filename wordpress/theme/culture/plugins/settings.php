<?php

if( is_admin() ) $apparaitreSettings = new ApparaitreSettings();


class ApparaitreSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        /*
        add_options_page(
            'Settings Admin', 
            'My Settings', 
            'manage_options', 
            'identite-site', 
            array( $this, 'apparaitre_settings' )
        );
        */
        add_menu_page(
            "Configuration des variables locales", 
            "Paramètres culture", 
            'manage_options', 
            'identite-site', 
            array( $this, 'apparaitre_settings' )
        );
    }

    /**
     * Options page callback
     */
    public function apparaitre_settings()
    {
        // Set class property
        $this->options = get_option( 'identite_name' );
        ?>
        <style>
            textarea {
                height: 100px;
                width: 100%;
            }
        </style>

        
        <div class="wrap">
            <h1 class="hndle ui-sortable-handle"><span>Variables globales</span></h1>
            <div id="poststuff">
                <div id="identite-container-1" class="postbox-container">
                    <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                        <div  class="postbox ">
                            <button type="button" class="handlediv button-link" aria-expanded="true">
                                <span class="screen-reader-text">Ouvrir/fermer le bloc Extrait</span>
                                <span class="toggle-indicator" aria-hidden="true"></span>
                            </button>
                            <h2 class="hndle ui-sortable-handle"><span>Identité du site</span></h2>
                            <div class="inside">
                                <div id="post-body" class="metabox-holder columns-2">
                                    <div id="post-body-content" style="position: relative;">
                                        <form method="post" action="options.php">
                                            <?php settings_fields('identite_group');?>
                                            <?php do_settings_sections( 'identite-site' );?>
                                            <?php submit_button(); ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'identite_group', // Option group
            'identite_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'identite_section_id', // ID
            '', // Title
            array( $this, 'print_section_info' ), // Callback
            'identite-site' // Page
        );  

        add_settings_field(
            'page_facebook', // ID
            'Page facebook', // Title 
            array( $this, 'page_facebook_callback' ), // Callback
            'identite-site', // Page
            'identite_section_id' // Section           
        );

        add_settings_field(
            'page_twitter', // ID
            'Compte twitter', // Title 
            array( $this, 'page_twitter_callback' ), // Callback
            'identite-site', // Page
            'identite_section_id' // Section           
        );

        add_settings_field(
            'page_youtube', // ID
            'Chaîne youTube', // Title 
            array( $this, 'page_youtube_callback' ), // Callback
            'identite-site', // Page
            'identite_section_id' // Section           
        );
        
        add_settings_field(
            'email_culture', // ID
            'Email responsable culture', // Title 
            array( $this, 'email_culture_callback' ), // Callback
            'identite-site', // Page
            'identite_section_id' // Section           
        ); 
        
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        /*
        $new_input = array();
        $new_input['adresse'] = sanitize_text_field( $input['adresse'] );

        return $new_input;
        */
        return $input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print '';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function adresse_callback()
    {
        $value = isset( $this->options['adresse'] ) ? $this->options['adresse'] : '';
      

        $settings = array(
            'textarea_name' => 'identite_name[adresse]',
            'media_buttons' => false,
            'tinymce' => array(
                'theme_advanced_buttons1' => 'formatselect,|,bold,italic,underline,|,bullist,blockquote,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,|,spellchecker,wp_fullscreen,wp_adv'
            )
        );

        wp_editor( $value, 'adresse', $settings );
    }


    public function page_facebook_callback()
    {
        $value = isset( $this->options['page_facebook'] ) ? esc_attr( $this->options['page_facebook']) : '';
        echo '<input type="text" style="width:100%"  id="page_facebook" name="identite_name[page_facebook]" value="' . $value . '" /">';
    }
    public function page_twitter_callback()
    {
        $value = isset( $this->options['page_twitter'] ) ? esc_attr( $this->options['page_twitter']) : '';
        echo '<input type="text" style="width:100%"  id="page_twitter" name="identite_name[page_twitter]" value="' . $value . '" /">';
    }
    public function page_youtube_callback()
    {
        $value = isset( $this->options['page_youtube'] ) ? esc_attr( $this->options['page_youtube']) : '';
        echo '<input type="text" style="width:100%"  id="page_youtube" name="identite_name[page_youtube]" value="' . $value . '" /">';
    }

    public function email_culture_callback()
    {
        $value = isset( $this->options['email_culture'] ) ? esc_attr( $this->options['email_culture']) : '';
        echo '<input type="text" style="width:100%"  id="email_culture" name="identite_name[email_culture]" value="' . $value . '" /">';
    }

}