<form action="<?php $form_action; ?>" method="POST" class="comment-form">
	<?php wp_nonce_field('inscription_evenement', 'inscription_evenement-verif'); ?>
	<div class="form-group">
		<label for="nom">Je m'inscris pour l'évènement:</label>
		<p><?php echo $event_title; ?></p>
		<input  type="hidden" name="event_title" value="<?php echo $event_title; ?>" />
		<input  type="hidden" name="event_url" value="<?php echo $event_url; ?>" />

	</div>

	<div class="form-group">
		<label for="nom">Votre nom</label>
		<input id="nom" type="text" name="nom" value="" class="form-control" />
	</div>

	<div class="form-group">
		<label for="email">Votre email</label>
		<input id="email" type="text" name="email" value="" class="form-control" />
	</div>

	<div class="form-group">
		<label for="telephone">Votre téléphone</label>
		<input id="telephone" type="text" name="telephone" value="" class="form-control" />
	</div>

	<p>
		<input id="submit" type="submit" name="inscription_evenement" id="submit" class="submit" value="Envoyer" />
	</p>
</form>