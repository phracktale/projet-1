<?php



function traitement_formulaire_inscription_evenement() {
	if (isset($_GET['erreur']))
	{
		switch('erreur')
		{
			case "email":
				return '<div class="alert alert-success">Il y a eu un problème d\'envoi d\'email</div>';
				break;
		}
	}
	if (isset($_POST['inscription_evenement']) && isset($_POST['inscription_evenement-verif']))  {
		if (wp_verify_nonce($_POST['inscription_evenement-verif'], 'inscription_evenement')) 
		{
			$hasError = false;
			if(trim($_POST['email']) === '') {
				$nameError = 'Veuillez entrer un email valide';
				$hasError = true;
			} else {
				$email = sanitize_email($_POST['email']);
			}

			$event_title = sanitize_text_field($_POST['event_title']);
			$event_url = esc_url($_POST['event_url']);
			$nom = sanitize_text_field($_POST['nom']);
			$telephone = sanitize_text_field($_POST['telephone']);
			$nb_inscrits = sanitize_text_field($_POST['nb_inscrits']);

			$emailTo = get_option('email_culture');
			if (!isset($emailTo) || ($emailTo == '') ){
				$emailTo = get_option('admin_email');
			}
			$subject = '[Inscription évènement] ' . $event_title;
			$body = "Bonjour,\nUne personne s'est inscrite à un de vos évènement sur votre site.\n\nVoici ses coordonnées:\n\nNom: $nom \nEmail: $email\Téléphone: $telephone \n\nEvènement auquel cette personne désire s'inscrire:\n $event_title \n" . $event_url .'\n\nNombre de personne désirant s\'inscrire: \n\n' ;
			
			$headers = "From: culture le plessis-belleville <noreply@phracktale.com>\r\nReply-To: " . $email;

			if(!wp_mail($emailTo, $subject, $body, $headers))
			{
				$url = add_query_arg('erreur', 'email', wp_get_referer());
				wp_safe_redirect($url);
				exit();
			}
			$emailSent = true;

			return '<div class="alert alert-success">Merci, votre inscription a bien été envoyée.</div>';
				
		}
	}
}