<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="layout has-gutter">
			<div id="content">
				<header>
					<h2><span class="glyph glyph-points"></span><?php echo sprintf( __( '%s Résultat de recherche pour ', 'culture' ), $wp_query->found_posts ); echo get_search_query(); ?></h2>
					
				</header>
				<div>
					<div class="items-list">
						<?php get_template_part('loop'); ?>
						<?php get_template_part('pagination'); ?>
					</div>
				</div>
			</div>

			<aside>
				<?php  dynamic_sidebar('widget-aside'); ?>		
			</aside>
		</section>
		<!-- /section -->
	</main>
<?php get_footer(); ?>