<!doctype html>
<html class="no-js" lang="fr">
	<head>
		<meta charset="UTF-8">
		<title>Le Plessis BELLEVILLE - Saison culturelle 2017/2018</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<base href="<?php echo home_url(); ?>" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<style type="text/css">
			@charset 'UTF-8';img,label{vertical-align:middle}#navigation li,.inbl{vertical-align:top}html{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;box-sizing:border-box;font-size:62.5%;font-size:calc(1em*.625)}body{margin:0;font-size:1.6rem;line-height:1.5;color:#000;background-color:#fff}header,hr,nav{display:block}a{background-color:transparent;-webkit-text-decoration-skip:objects;color:#333}b{font-weight:bolder}h1{font-size:3.2rem}img{border-style:none;max-width:100%;height:auto}hr{overflow:visible;box-sizing:content-box;clear:both;height:1px;margin:1em 0 2em;padding:0;color:#ccc;border:0;background-color:#ccc}::-webkit-input-placeholder{opacity:.54;color:#777}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}*{box-sizing:inherit}ul{padding-left:2em}label,ul{line-height:1.5;margin-top:.75em;margin-bottom:0}h2{font-size:2.8rem}i{font-style:italic}h2:first-child,ul:first-child{margin-top:0}@media (max-width:33.9375em){div{word-wrap:break-word;-webkit-hyphens:auto;-ms-hyphens:auto;hyphens:auto}}@media screen and (-ms-high-contrast:active),(-ms-high-contrast:none){img[src$='.svg']{width:100%}}label{font-family:inherit;font-size:inherit}.inbl{display:inline-block}@font-face{font-family:Raleway;font-weight:400;font-style:normal;src:local('Raleway'),local('Raleway-Regular'),url(http://fonts.gstatic.com/s/raleway/v11/0dTEPzkLWceF7z0koJaX1A.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2212,U+2215,U+E0FF,U+EFFD,U+F000}@font-face{font-family:Raleway;font-weight:700;font-style:normal;src:local('Raleway Bold'),local('Raleway-Bold'),url(http://fonts.gstatic.com/s/raleway/v11/JbtMzqLaYbbbCL9X6EvaI_k_vArhqVIZ0nv9q090hN8.woff2) format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2212,U+2215,U+E0FF,U+EFFD,U+F000}body,html{font-family:Raleway;font-weight:400;overflow-x:hidden;-webkit-overflow-scrolling:touch}body{width:100%;border-top:4px solid #4ca244;background:#fff}.full-width{width:100vw;margin-right:0;margin-left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}.full-width .wrapper{background-color:transparent}#page,.full-width .wrapper{width:75em;margin:0 auto}h1,h2{line-height:2;margin:0}h2{font-weight:300;text-transform:uppercase}.hide{display:none}#header{position:relative;min-height:8.875em;padding-top:1em;background:#fff;background-size:cover}#header a.mairie{font-size:.8em;float:left;padding:1em;text-decoration:none;opacity:.5;color:#fff;border-radius:4px;background:#4ca244}#header h1{display:block;width:500px;margin-bottom:1.25em;margin-left:auto;padding-top:3em}#navigation li,#navigation li a,.glyph{display:inline-block}#header h1 img{width:100%}#navigation{background-color:#666}#navigation ul{margin-top:0;padding-left:0}#navigation li a{line-height:3.5em;padding:.5em 1em .6em;text-decoration:none;color:#fff}#navigation li:first-child a span{padding-left:3em}#navigation .logo-nav img{height:4.5em;padding:0}.glyph{width:2em;height:2em;margin-right:1em;background-size:cover}.glyph-home{position:absolute;margin-top:.5em;background-image:url(<?php echo get_template_directory_uri() . "/"; ?>assets/img/home.svg)}@media (max-width:75em){#page,.full-width .wrapper{width:100%;margin:0}}@media (min-width:34em) and (max-width:47.9375em){h1 img{max-width:100%}}@font-face{font-family:Raleway;font-style:normal;font-weight:400;src:local('Raleway'),local('Raleway-Regular'),url(http://fonts.gstatic.com/s/raleway/v11/bIcY3_3JNqUVRAQQRNVteQ.ttf) format('truetype')}@font-face{font-family:Raleway;font-style:normal;font-weight:700;src:local('Raleway Bold'),local('Raleway-Bold'),url(http://fonts.gstatic.com/s/raleway/v11/JbtMzqLaYbbbCL9X6EvaIy3USBnSvpkopQaUR-2r7iU.ttf) format('truetype')}
		</style>
		<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/assets/css/styles.min.css" media="all" as="style" onload="this.rel='stylesheet'">
		<noscript><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>a/ssets/css/styles.min.css" media="all"></noscript>
		<link rel="preload" href="//fonts.googleapis.com/css?family=Raleway:400,700" media="all" as="style" onload="this.rel='stylesheet'">
		<noscript><link rel="stylesheet" href="//fonts.googleapis.com/css?family=Raleway:400,700" media="all"></noscript>
		<script>!function(e){"use strict";var t=function(t,n,r){function o(e){return i.body?e():void setTimeout(function(){o(e)})}function a(){d.addEventListener&&d.removeEventListener("load",a),d.media=r||"all"}var l,i=e.document,d=i.createElement("link");if(n)l=n;else{var s=(i.body||i.getElementsByTagName("head")[0]).childNodes;l=s[s.length-1]}var u=i.styleSheets;d.rel="stylesheet",d.href=t,d.media="only x",o(function(){l.parentNode.insertBefore(d,n?l:l.nextSibling)});var f=function(e){for(var t=d.href,n=u.length;n--;)if(u[n].href===t)return e();setTimeout(function(){f(e)})};return d.addEventListener&&d.addEventListener("load",a),d.onloadcssdefined=f,f(a),d};"undefined"!=typeof exports?exports.loadCSS=t:e.loadCSS=t}("undefined"!=typeof global?global:this),function(e){if(e.loadCSS){var t=loadCSS.relpreload={};if(t.support=function(){try{return e.document.createElement("link").relList.supports("preload")}catch(e){return!1}},t.poly=function(){for(var t=e.document.getElementsByTagName("link"),n=0;n<t.length;n++){var r=t[n];"preload"===r.rel&&"style"===r.getAttribute("as")&&(e.loadCSS(r.href,r),r.rel=null)}},!t.support()){t.poly();var n=e.setInterval(t.poly,300);e.addEventListener&&e.addEventListener("load",function(){e.clearInterval(n)}),e.attachEvent&&e.attachEvent("onload",function(){e.clearInterval(n)})}}}(this);</script>
		<meta name="description" content="La culture au plessis-belleville">
	</head>
	<body>
		<div id="page">
<ul class="lienAccess hide">
	<li><a href="/">Accueil</a></li>
	<li><a href="#menu">Menu</a></li>
	<li><a href="#contenu">Contenu</a></li>
	<li><a href="#champRecherche">Recherche</a></li>
	<li><a class="accessibilite" href="100-accessibilite.html">Accessibilité</a></li>
</ul>
			<hr class="hide">
		<header id="header" role="banner" class="full-width" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/img/header.jpg)">
			<div class="wrapper">
				<div class="preheader">
				<a href="http://www.leplessisbelleville.fr/" class="mairie" title="Accès mairie">
					Le Plessis<br><b>BELLEVILLE</b>
				</a>
				</div>
					<h1>
						<a href="01-accueil.html">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-culture.svg" alt="le PLessis-Belleville - Activités culturelles">
						</a>
					</h1>
		
		<nav id="navigation" role="navigation" class="full-width">
			<div class="wrapper">
				<div class="grid">
					<div>
						<a name="menu" class="hide"><h2>Menu de navigation principal</h2></a>
						<a href="#" class="link-menu-mobile mobile"><span></span></a>
						<div id="mobile-search" class="mobile">
							<?php get_template_part('search-form'); ?>
						</div>
						<?php culture_nav('menu-principal'); ?>
						
						<hr class="hide">
					</div>
					<div class="desktop one-third">
						<div class="grid txtright">
							<span id="bar-search">
							<?php get_template_part('search-form'); ?>
							</span>
							<a href="01-accueil.html" class="logo-nav hide">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-culture-nav.svg" alt="le PLessis-Belleville - Activités culturelles">
							</a>
						</div>
						<hr class="hide">
					</div>
				</div>				
			</div>
		</nav>
				<hr class="hide">
			</div>

		</header>
			<main role="main">
				<a name="contenu" class="hide"><h2>Contenu</h2></a>
				<hr class="hide">