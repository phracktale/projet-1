<?php
/**
 * Template Name: Actualités
 */
?>
<?php get_header(); ?>

	<main role="main">
		<!-- SLIDER -->
			<?php  
				
				 while ( have_posts() ) : the_post();
					$post_id = get_the_ID();
					set_query_var( 'post_id', $post_id ); 
					get_template_part( 'blocks/page-slider', null ); 
				endwhile;
			?>

		<!-- section -->
		<section class="layout has-gutter">
			<div id="content">
				<header>
					<?php while ( have_posts() ) : the_post(); ?>
					<h2><span class="glyph glyph-points"></span><?php echo the_title(); ?></h2>
					<?php echo the_content(); ?>
					<?php endwhile; // end of the loop. ?>
				</header>
				<div>
					<div class="items-list">
						<?php 
							$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

							$args = array(
								'posts_per_page' => 5	,
								'paged' => $paged
							);

							query_posts( $args); 
						?>
						<?php get_template_part('loop'); ?>
						<?php get_template_part('pagination'); ?>
					</div>
				</div>
			</div>

			<aside>
				<?php  dynamic_sidebar('widget-aside'); ?>
			</aside>
		</section>
		<!-- /section -->
	</main>
<?php get_footer(); ?>