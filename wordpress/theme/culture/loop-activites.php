<?php $current_url = get_the_permalink(); ?>
<div class="grid has-gutter">
	<div class=" one-sixth date">
		<div class="card">
		 	<p><?php echo date_i18n('d F', strtotime(get_post_meta($post->ID, 'date_event', true))); ?></p>
		 	<p class="an"><?php echo date_i18n('Y', strtotime(get_post_meta($post->ID, 'date_event', true))); ?></p>
		 </div>
	</div>
	<div class="two-thirds presentation">
		<?php the_post_thumbnail(array(200,200), array( 'class' => 'logo' )); // Declare pixel size you need inside the array ?>
		<h3><?php the_title(); ?></h3>
		<?php the_content(); ?>
		<div id="modal-inscription-<?php the_ID(); ?>" class="modal"  style="display:none;">
			<?php
				set_query_var( 'event_title', get_the_title() ); 
				set_query_var( 'event_url', get_the_permalink() ); 
				set_query_var( 'form_action', $current_url ); 
				get_template_part( 'plugins/inscription/form-signin', null );
			?>
		</div>
	</div>
	<div class="two-sixth txtcenter inscription">
		<a href="#modal-inscription-<?php the_ID(); ?>" rel="modal:open" data-event="<?php the_title(); ?>" class="btn btn-primary mtl	"><?php _e( 'Je m\'inscris', 'culture' ); ?></a>
	</div>
	
</div>