(function() {
    tinymce.create('tinymce.plugins.culture', {
        init : function(ed, url) {
            ed.addButton('bloc', {
                title : 'Ajouter un bloc',
                image : url+'/shortcode_bloc.png',
                onclick : function() {
                    ed.windowManager.open({
                        title: 'Ajouter un bloc',
                        body: [{
                            type: 'textbox',
                            name: 'titre',
                            label: 'Titre'
                        },{
                            type: 'combobox',
                            name: 'fonction',
                            label: 'Fonction',
                            values : [
                                { text: 'Article', value: 'article' },
                                { text: 'Catégorie', value: 'categorie' }
                            ]
                        },{
                            type: 'combobox',
                            name: 'display',
                            label: 'Disposition',
                            values : [
                                { text: 'En grille', value: 'grid' },
                                { text: 'En liste', value: 'list' }
                            ]
                        },{
                            type: 'combobox',
                            name: 'post_type',
                            label: 'Type de contenu',
                            values : [
                                { text: 'Articles', value: 'post' },
                                { text: 'Activités', value: 'activites' }
                            ]
                        },{
                            type: 'textbox',
                            name: 'col',
                            label: 'Nb d\'items'
                        },{
                            type: 'combobox',
                            name: 'affiche_description',
                            label: 'Afficher la description',
                            values : [
                                { text: 'Oui', value: 'oui' },
                                { text: 'Non', value: 'non' }
                            ]
                        },{
                            type: 'combobox',
                            name: 'lien_vignette',
                            label: 'Vignette cliquable ?',
                            values : [
                                { text: 'Oui', value: 'oui' },
                                { text: 'Non', value: 'non' }
                            ]
                        },{
                            type: 'textbox',
                            name: 'lien',
                            label: 'Bouton: URL cible'
                        },{
                            type: 'textbox',
                            name: 'texte_lien',
                            label: 'Bouton: Texte du lien'
                        },{
                            type: 'combobox',
                            name: 'animation',
                            label: 'Animation',
                            values : [
                                { text: 'Pas d\'animation', value: 'fixe' },
                                { text: 'Zoom Vignette', value: 'zoom' }
                            ]
                        }],
                        onsubmit: function( e ) {
                            ed.insertContent( '[bloc fonction="' + e.data.fonction + '" display="' + e.data.display + '" post_type="' + e.data.post_type + '" col="' + e.data.col + '" titre="' + e.data.titre + '" affiche_description="' + e.data.affiche_description + '" lien_vignette="' + e.data.lien_vignette + '" lien="' + e.data.lien + '" texte_lien="' + e.data.texte_lien + '" animation="' + e.data.animation + '"]' + ed.selection.getContent() + '[/bloc]' );
                        }
                    });
 
                }
            });

            ed.addButton('separateur', {
                title : 'Ajouter un séparateur',
                image : url + '/shortcode_separateur.png',
                onclick : function() {
                   ed.selection.setContent( ed.selection.getContent() + '[separateur/]');
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('culture', tinymce.plugins.culture);
})();